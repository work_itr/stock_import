Candidate development test. CSV-to-DB parser.
=====

**Installation**

Clone this repository to your suitable directory like this:
git clone https://jaro1989@bitbucket.org/work_itr/stock_import.git

Move to your project directory in command line and write `composer update`

Import make_database.sql to your database and than migrate to last available version using doctrine migration bundle.
Some tips are here: http://symfony.com/doc/master/bundles/DoctrineMigrationsBundle/index.html

And...that's it.

**Usage**

Open your terminal, move to your project directory and write the following:
`php bin/console import:stock "/path/to/csv/" "filename.csv" --test(optional)`

 Example:` php bin/console stock:import /var/www/test.com/public_html/stock/ stock.csv --test`


**Tests**

Please, change paths to your csv-files to make tests work. And remove 'dist' from phpunit.xml.dist file extension.





