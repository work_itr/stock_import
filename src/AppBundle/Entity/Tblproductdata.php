<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tblproductdata
 *
 * @UniqueEntity("strproductcode")
 */
class Tblproductdata
{
    /**
     * @var integer
     */
    private $intproductdataid;

    /**
     * @var string
     */
    private $strproductname;

    /**
     * @var string
     */
    private $strproductdesc;

    /**
     * @var string
     */
    private $strproductcode;

    /**
     * @var \DateTime
     */
    private $dtmadded;

    /**
     * @var \DateTime
     */
    private $dtmdiscontinued;

    /**
     * @var \DateTime
     */
    private $stmtimestamp;

    /**
     *
     * @Assert\GreaterThan(value="10")
     * @var integer
     */
    private $intStock;

    /**
     *
     * @Assert\GreaterThan(value="5")
     * @Assert\LessThan(value="1000")
     *
     * @var float
     */
    private $doublePrice;

    /**
     * @return int
     */
    public function getIntproductdataid()
    {
        return $this->intproductdataid;
    }

    /**
     * @return string
     */
    public function getStrproductname()
    {
        return $this->strproductname;
    }

    /**
     * Set strproductname
     *
     * @param string $strproductname
     *
     * @return Tblproductdata
     */
    public function setStrproductname($strproductname)
    {
        $this->strproductname = $strproductname;

        return $this;
    }

    /**
     * @return string
     */
    public function getStrproductdesc()
    {
        return $this->strproductdesc;
    }

    /**
     * Set strproductdesc
     *
     * @param string $strproductdesc
     *
     * @return Tblproductdata
     */
    public function setStrproductdesc($strproductdesc)
    {
        $this->strproductdesc = $strproductdesc;

        return $this;
    }

    /**
     * @return string
     */
    public function getStrproductcode()
    {
        return $this->strproductcode;
    }

    /**
     * Set strproductcode
     *
     * @param string $strproductcode
     *
     * @return Tblproductdata
     */
    public function setStrproductcode($strproductcode)
    {
        $this->strproductcode = $strproductcode;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtmadded()
    {
        return $this->dtmadded;
    }

    /**
     * Set dtmadded
     *
     * @param \DateTime $dtmadded
     *
     * @return Tblproductdata
     */
    public function setDtmadded($dtmadded = NULL)
    {
        $this->dtmadded = $dtmadded;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDtmdiscontinued()
    {
        return $this->dtmdiscontinued;
    }

    /**
     * Set dtmdiscontinued
     *
     * @param \DateTime $dtmdiscontinued
     *
     * @return Tblproductdata
     */
    public function setDtmdiscontinued($dtmdiscontinued = NULL)
    {
        $this->dtmdiscontinued = $dtmdiscontinued;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStmtimestamp()
    {
        return $this->stmtimestamp;
    }

    /**
     * Set stmtimestamp
     *
     * @param \DateTime $stmtimestamp
     *
     * @return Tblproductdata
     */
    public function setStmtimestamp($stmtimestamp)
    {
        $this->stmtimestamp = $stmtimestamp;

        return $this;
    }

    /**
     * @return int
     */
    public function getIntStock()
    {
        return $this->intStock;
    }

    /**
     * Set intStock
     *
     * @param integer $intStock
     *
     * @return Tblproductdata
     */
    public function setIntStock($intStock)
    {
        $this->intStock = $intStock;

        return $this;
    }

    /**
     * @return float
     */
    public function getDoublePrice()
    {
        return $this->doublePrice;
    }

    /**
     * Set doublePrice
     *
     * @param float $doublePrice
     *
     * @return Tblproductdata
     */
    public function setDoublePrice($doublePrice)
    {
        $this->doublePrice = $doublePrice;

        return $this;
    }
}

