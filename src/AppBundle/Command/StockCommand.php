<?php
// src/AppBundle/Command/StockCommand.php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class StockCommand extends ContainerAwareCommand
{
    const INSERT_MODE = 0;
    const TEST_MODE = 1;

    public $stockColumns = ['Product Code', 'Product Name', 'Product Description', 'Stock', 'Cost in GBP', 'Discontinued'];

    protected $mode;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {

        $this
            ->setName('stock:import')
            ->setDescription('Export CSV-file')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Input path to your CSV file'
            )
            ->addArgument(
                'filename',
                InputArgument::REQUIRED,
                'Input name of CSV file'
            )
            ->addOption(
                'test',
                null,
                InputOption::VALUE_NONE,
                'If set, no insertion will be proceed'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $filename = $input->getArgument('filename');

        if ($input->getOption('test')) {
            $this->mode = self::TEST_MODE;
        } else {
            $this->mode = self::INSERT_MODE;
        }

        $csv = $this->getContainer()->get('csv');
        $parseResult = $csv->parseCSV($path, $filename, $this->stockColumns);

        $data = $parseResult->getData();
        $report = $parseResult->getReport();

        if ($this->mode === self::INSERT_MODE) {
            $report = $csv->validateData($data, $report);
            $report = $csv->importData($data, $report);
        } else {
            $report = $csv->validateData($data, $report);
        }

        /*
         * Displaying of parse info
         */
        $formatter = $this->getHelper('formatter');

        $formatterLineProcessed = $formatter->formatSection('Processed products', $report->getProcessedCount());
        $formatterLineSkipped = $formatter->formatSection('Skipped products', $report->getFailureCount(), 'error');
        $formatterLineSuccessful = $formatter->formatSection('Successful products', $report->getSuccessCount());

        $output->writeln($formatterLineProcessed);
        $output->writeln($formatterLineSkipped);
        $output->writeln($formatterLineSuccessful);
        $output->writeln(' ' . PHP_EOL);

        foreach ($report->getErrors() as $key => $message) {
            $formatterLineErrorInfo = $formatter->formatSection($key, $message, 'error');
            $output->writeln($formatterLineErrorInfo);
        }
    }
}
