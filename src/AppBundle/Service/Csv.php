<?php

namespace AppBundle\Service;

use AppBundle\Service\Csv\ParseResult;
use AppBundle\Service\Csv\ProductMapper;
use AppBundle\Service\Csv\StockReport;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class Csv implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $report;

    /**
     * @var StockReport
     */

    public function __construct()
    {
        $this->report = new StockReport();
    }

    /**
     * Converts csv file to array
     *
     * *******CSV-file******************
     * header1;header2
     * value1;value2
     * value3;value4
     * *******To array******************
     * [['header1' => 'value1', 'header2' => 'value2'],
     * ['header1' => 'value3', 'header2' => 'value4']]
     * *********************************
     * @param string $path
     * @param string $filename
     * @param $columns
     * @param $separator
     *
     * @throws \Exception
     * @return ParseResult
     */
    public function parseCSV($path, $filename, array $columns, $separator = ',')
    {
        $report = new StockReport();

        $finder = new Finder();
        $file = $finder->files()
            ->in($path)
            ->name($filename)->getIterator();
        $file->rewind();
        $csv = $file->current();

        if (isset($csv)) {

            $file = fopen($csv, "r");
            $productData = [];
            $i = 0;
            while (($data = fgetcsv($file, 0, $separator)) !== FALSE) {
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    $productData[$i][] = $data[$c];
                }
                $i++;
            }
            fclose($file);
            $keys = array_shift($productData);

            /*
             * Check for a right keys
             */
            if (array_diff($columns, $keys) !== [] && array_diff($keys, $columns) !== []) {
                throw new \Exception('CSV format isn\'t correct');
            }

            $report->setProcessedCount(count($productData));

            /*
             * Saving CSV info to array
             */
            foreach ($productData as $key => $row) {
                try {
                    $productData[$key] = array_combine($keys, $row);
                } catch (\Exception $e) {
                    $report->increaseFailure();
                    $report->addError($key, 'Wrong format of row', 'Row #');
                    unset($productData[$key]);
                }
            }
        } else {
            throw new \Exception('Path to your file isn\'t correct. Please, check it.');
        }

        $products = [];
        $mapper = new ProductMapper();

        foreach ($productData as $productRow) {
            $products[] = $mapper->getObject($productRow);

        }

        $result = new ParseResult();
        $result->setReport($report);
        $result->setData($products);

        return $result;
    }

    /**
     * Inserting CSV-data to DB using Doctrine Entity Manager
     *
     * @param array $data
     * @param StockReport $report
     * @return StockReport $report
     */
    public function importData(array $data, StockReport $report = null)
    {
        if (!$report) {
            $report = new StockReport();
        }

        foreach ($data as $product) {
            $em = $this->getContainer()->get('doctrine')->getManager();

            try {
                if (!$report->isHasError($product->getStrproductcode())) {
                    $em->persist($product);
                    $em->flush();
                }
            } catch (\Exception $e) {
                $report->increaseFailure();
                $report->addError($product->getStrproductcode(), 'SQL failed: ' . $e->getMessage(), 'Product Code #');

                $this->getContainer()->get('doctrine')->resetManager(); //Prevent Entity Manager closing before end of script
            }
        }

        return $report;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     * @param  $report
     *
     * @return StockReport
     */
    public function validateData(array $data, StockReport $report = null)
    {
        if (!$report) {
            $report = new StockReport();
        }

        foreach ($data as $product) {
            /*
             * Validation block
             */
            $validator = $this->getContainer()->get('validator');
            $errors = $validator->validate($product);

            if (count($errors) > 0) {
                $report->increaseFailure();
                $report->addError($product->getStrproductcode(), 'Validation failed', 'Product Code #');

            } else {
                $report->increaseSuccess();
            }
        }

        return $report;
    }
}
