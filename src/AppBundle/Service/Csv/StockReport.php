<?php

namespace AppBundle\Service\Csv;

class StockReport
{
    //protected $importInfo;
    protected $messages = [];
    protected $itemsValidation = [];

    protected $successCount = 0;
    protected $failureCount = 0;
    protected $processedCount = 0;

    /**
     * @param string $code
     * @param string $message
     * @param string $prefix
     */
    public function addError($code, $message, $prefix)
    {
        if (!isset($this->messages[$code])) {
            $this->messages[$code] = [];
        }
        
        $this->messages[$code][] = [$message, $prefix, true];
    }


    public function increaseSuccess()
    {
        $this->successCount++;
    }

    public function increaseFailure()
    {
        $this->failureCount++;
    }

    /**
     * @return mixed
     */
    public function getSuccessCount()
    {
        return $this->successCount;
    }

    /**
     * @return mixed
     */
    public function getFailureCount()
    {
        return $this->failureCount;
    }

    /**
     * @return mixed
     */
    public function getProcessedCount()
    {
        return $this->processedCount;
    }

    /**
     * @param mixed $processedCount
     */
    public function setProcessedCount($processedCount)
    {
        $this->processedCount = $processedCount;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param array $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        $result = array();
        foreach ($this->messages as $code => $messages) {
            foreach ($messages as $message) {
                if ($message[2]) {
                    if (!isset($result[$code])) {
                        $result[$code] = [];
                    }

                        $result[$code][] = sprintf('%s: %s', $message[1], $message[0]);
                }
            }
        }

        foreach ($result as $code => $messages) {
            $result[$code] = implode('; ', $messages);
        }

        return $result;
    }

    /**
     * @param string $code
     * @return bool
     */
    public function isHasError($code)
    {
        if (isset($this->messages[$code])) {
            foreach ($this->messages[$code] as $message) {
                if ($message[2]) {
                    return true;
                }
            }
        }

        return false;
    }
}