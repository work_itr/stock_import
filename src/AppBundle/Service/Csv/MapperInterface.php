<?php

namespace AppBundle\Service\Csv;

interface MapperInterface
{
    public function getObject(array $data);
}