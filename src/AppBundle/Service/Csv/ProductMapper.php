<?php

namespace AppBundle\Service\Csv;

use AppBundle\Entity\Tblproductdata;

class ProductMapper implements MapperInterface
{
    public function getObject(array $data)
    {
        $product = new Tblproductdata();

        $product->setStrproductname($data['Product Name']);
        $product->setDtmadded(new \DateTime('now'));
        if ($data['Discontinued'] === 'yes') {
            $product->setDtmdiscontinued(new \DateTime('now'));
        } else {
            $product->setDtmdiscontinued();
        }
        $product->setStmtimestamp(new \DateTime());
        $product->setStrproductcode($data['Product Code']);
        $product->setStrproductdesc($data['Product Description']);
        $product->setIntStock($data['Stock']);
        $product->setDoublePrice($data['Cost in GBP']);

        return $product;
    }
}