<?php

namespace AppBundle\Service\Csv;

class ParseResult
{
    /**
     * @var StockReport
     */
    protected $report;
    /**
     * @var array
     */
    protected $data;

    /**
     * @return StockReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param StockReport $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}
