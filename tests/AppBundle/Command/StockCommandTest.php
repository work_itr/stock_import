<?php

namespace Tests\AppBundle\Controller;


use AppBundle\Command\StockCommand;
use AppBundle\Service\Csv;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class StockCommandTest extends KernelTestCase
{
    /*
     * @var Application
     */
    public $path;
    public $filename;
    private $application;

    /**
     * Testing of displaying correct answer
     * @dataProvider provider
     */
    public function testExecute($mode)
    {
        $command = $this->application->find('stock:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'path' => $this->path,
            'filename' => $this->filename,
            '--test' => $mode

        ]);

        $this->assertFileExists($this->path . '/' . $this->filename);
        $this->assertRegExp('/Successful/', $commandTester->getDisplay());
        $this->assertRegExp('/Processed/', $commandTester->getDisplay());
        $this->assertRegExp('/Skipped/', $commandTester->getDisplay());

    }

    /**
     * Get Exception when path to csv-file isn't correct
     * @expectedException \Exception
     *
     */
    public function testExceptions()
    {
        $command = $this->application->find('stock:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'path' => $this->path,
            'filename' => 'wrong.csv',
            '--test' => 1

        ]);

    }

    /**
     *
     * Gets Exception when CSV isn't suitable for our DB-structure
     * @expectedException \Exception
     *
     */
    public function testBadFormat()
    {
        $command = $this->application->find('stock:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'path' => $this->path,
            'filename' => 'stock_test2.csv',
            '--test' => 1

        ]);

    }


    public function provider()
    {
        return [
            [1],
            [0],
        ];
    }

    public function setUp()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $this->application = new Application($kernel);
        $this->application->add(new StockCommand());
        /*
         * Set up for your environment
         */
        $this->path = $kernel->getRootDir() . '/TestData/';
        $this->filename = 'stock.csv';

        $this->setVerboseErrorHandler();
    }

    protected function setVerboseErrorHandler()
    {
        $handler = function ($errorNumber, $errorString, $errorFile, $errorLine) {
            echo "
                    ERROR INFO
                    Message: $errorString
                    File: $errorFile
                    Line: $errorLine
            ";
        };
        set_error_handler($handler);
    }

}